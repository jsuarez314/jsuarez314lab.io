---
layout: project_single
title:  "RECA Mentorship"
categories: projects
---


<span style="display:block;text-align:center">![RECA](/assets/images/reca_mentorship.png)</span>

The Network of Colombian Astronomy Students (RECA) is an association that helps to create and mantain the link between Colombian astronomy students around the world. As part of the network, I am involved in both the mentorship program (RECA mentorship) that focouses in guiding students to pursue a professional carrier in Astronomy and the organizing committee 2022-2023.

For more information about the different activities carried out by RECA visit: [RECA website]. 


[RECA website]: https://www.astroreca.org/
