---
layout: desiout_layout
title:  "Identifying DESI Outliers"
categories: projects
---

The spectra displayed on this webpage were identified as outliers using a specific method that is described in detail [here](/projects/umap_outliers). These outliers were detected during the observation night of 06th July 2021 (20210706). Here, you have the opportunity to inspect each spectrum individually and report any features that you identify as contributing to its classification as an outlier.

The Figures below show the UMAP projections of the observations in the B,R,Z and BRZ bands. The points highlighted in black are the outliers observations identified with the method.

<div align=center>
<img src="/assets/images/desioutliers/20210706/iron/main/outliers/20210706_iron_main_b_nn45_md1.0_cosine_ll0.15_mm5.png" width="45%">
<img src="/assets/images/desioutliers/20210706/iron/main/outliers/20210706_iron_main_r_nn45_md1.0_cosine_ll0.15_mm5.png" width="45%">
<img src="/assets/images/desioutliers/20210706/iron/main/outliers/20210706_iron_main_z_nn45_md1.0_cosine_ll0.15_mm5.png" width="45%">
<img src="/assets/images/desioutliers/20210706/iron/main/outliers/20210706_iron_main_brz_nn45_md1.0_cosine_ll0.15_mm5.png" width="45%">
<br>
</div>

# Reporting abnormal features

Below, you will find a collection of spectra that have been identified as outliers by the method. To inspect a particular spectrum, simply click on its miniature image, and it will be zoomed in for closer examination. You can navigate between the spectra using the arrow buttons or the arrow keys on your keyboard. If you notice any particular features that you think might be important, please take a moment to report them to me by clicking on the "Report" button. This will allow you to send me a message detailing the feature(s) you have identified during your visual inspection, which can be invaluable for further analysis and research. Don't hesitate to report anything you find interesting or unusual! 


<div class="container">
  <div class="image-preview">
    <img src="" alt="">
    <div class="preview-details">
      <div id="desiout_desc"></div>
      <button>Report</button>
      <div class="image-navigation">
        <button class="previous-image">&lt;</button>
        <button class="next-image">&gt;</button>
      </div>
    </div>
  </div>

  <div class="url-list">
    {% for file in site.static_files %}
      {% if file.path contains '/images/desioutliers/20210706/iron/main/outliers/' %}
        {% if file.extname == '.jpg' or file.extname == '.jpeg' or file.extname == '.png' %}
          {% if file.path contains '_out.' %}
            {% assign filename = file.path %}
            {% assign parts1 = filename | split: '/' %}
            {% assign parts2 = parts1[7] | split: '_' %}
            {% assign date = parts2[0] %}
            {% assign release = parts2[1] %}
            {% assign survey = parts2[2] %}
            {% assign band = parts2[3] | upcase %}
            {% assign island = parts2[9] %}
            {% assign outlier = parts2[10] | split: '.' | first %}
            <div class="spectrum">
              <img class="img-desiout" src="{{ file.path }}" data-filename="{{ file.name }}">
              <div class="data-desiout"> Band:{{ band }} <br>ID:{{ island }}_{{ outlier }}</div>
            </div>
          {% endif %}
        {% endif %}
      {% endif %}
    {% endfor %}
  </div>
</div>
