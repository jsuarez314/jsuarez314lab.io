---
layout: project_single
title:  "Controlling correlations between pair of photons"
categories: projects
---

  In this work an experimental method is presented to construct a source of heralded single photons given their importance in different applications in quantum optics. The difficulty to obtain individual photons on demand, where the most common mechanism is to use isolated light emitters, is due to the low efficiency of this technique. As a solution to this problem, the most used mechanism is to produce single photons from a source of entangled photon pairs, where one of the photons (signal/idler) heralded the existence of its partner (idler/signal).
  
  <span style="display:block;text-align:center">![Joint Spectrum](/assets/images/joint_spectrum.png)</span>
  
In order to construct the source of heralded single photons, the characterization of two SPDC sources was performed, one source using a collinear type I crystal and the other source with a collinear type II crystal. The characterization of the sources was performed by measuring the second order temporal correlation function, G^2(t), and the frequency correlation from the measurement of the joint spectrum (JS). From the characterization of the mentioned sources, the type II SPDC source is employed to obtain the heralded single photons. In order to have a pure heralded single photon source the frequency and the spatial correlations between the photon pairs need to be broken. Spatial and spectral filters are used to achieve this goal. As a consequence the efficiency to herald the single photons decrease. The heralded single photon source is characterized by the purity and heralded efficiency factors as a function of the bandwidth of the spectral filters used when the spatial correlations are broken. The results obtained show the control of the spectral correlation and the behavior of the purity and the heralded efficiency factors.


For more information refers to this [paper][paper].


[paper]: https://doi.org/10.1364/JOSAB.387118
