---
layout: project_single
title:  "MANTRA and Deep-TAO data sets"
categories: projects
---

 
**MANTRA** (**MA**chi**N**e Learning reference lightcurves dataset for astronomical **TRA**nsient) is an annotated dataset of 4869 transient and 71207 non-transient object lightcurves built from the [Catalina Real Time Transient Survey][crts]. We provide public access on [github][mantra-ds] to this dataset as a plain text file to facilitate standardized quantitative comparison of astronomical transient event recognition algorithms. 

Some of the classes included in the dataset are: supernovae (SN), cataclysmic variables (CV), active galactic nuclei (AGN), high proper motion stars (HPM), blazars (BZ) and flares. MANTRA can be performed on the dataset to experiment with multiple data pre-processing methods, feature selection techniques and popular machine learning algorithms (Support Vector Machines, Random Forests and Neural Networks).

<span style="display:block;text-align:center">![MANTRA](/assets/images/mantra.png)</span>

In the Figure, cumulative number of lightcurves (expressed as a fraction) as a function of average magnitude (left) and number of data points in the lightcurve (right). This includes information for the three most representative classes (SN, CV, AGN) and the whole database (ALL).

<br>
<br>

**Deep-TAO** (**Deep**-learning **T**ransient **A**stronomical **O**bject) is an annotated set of 1249079 images that include 3807 transient and 12500 Non-Transient sequences built from the [Catalina Real Time Transient Survey][crts]. We publish Deep-TAO to provide a clean, open, and easy-to-use data set to benchmark deep learning architectures for transient classification. The transient classes included in Deep-TAO are blazars (BZ), active galactic nuclei (AGN), cataclysmic variables (CV), supernovae (SN), and other events of unknown nature. We provide public access to this data set as FITS files in two github repositories, one for transients and other for non-transients objects.


<span style="display:block;text-align:center">![TAO](/assets/images/tao.png)</span>

The Figure is a sample images. Each row corresponds to a sample of a different class. The temporal spacing between consecutive images varies for each example. Images were normalized for visualization.

For more information refers to [MANTRA paper][mantra] or TAO paper (soon).


[mantra]: https://iopscience.iop.org/article/10.3847/1538-4365/aba267/pdf
[mantra-ds]: https://github.com/MachineLearningUniandes/MANTRA
[tao]: https://iopscience.iop.org/article/10.3847/1538-4365/aba267/pdf
[crts]: http://crts.caltech.edu/