---
layout: project_single
title:  "OCRA-AI model"
categories: projects
---

With the OCRA-AI model, which stands for **Oc**cupational **R**epetitive **A**ction based on **A**rtificial **I**ntelligence models, we aim to automate the detection of ergonomic risk factors in the pharmaceutical packaging task, with a specific focus on the OCRA tool. OCRA is a widely accepted method for assessing the risk of musculoskeletal disorders associated with repetitive tasks. Our proposal entails the integration of OCRA with state-of-the-art artificial intelligence (AI) techniques, utilizing a deep learning model known as YOLO (You Only Look Once).
  
<span style="display:block;text-align:center">![OCRA AI](/assets/images/ocranet.png)</span>

The project unfolds in three main stages: data collection, labeling using LabelStudio for various hand grips, and the training of the YOLO model to identify grip types and recognize forced postures in the shoulders, elbows, and wrists during the packaging process. The current phase involves optimizing the model's training to enhance the detection of different grip types. This innovative approach aims to provide an automated and efficient solution for ergonomic risk assessment in real-world industrial settings, combining the established OCRA methodology with the capabilities of AI for improved accuracy and practical implementation.
  
For more information refers to this [paper][paper].

[OCRA]: https://www.ergonautas.upv.es/metodos/ocra/ocra-ayuda.php
[paper]: https://ieeexplore.ieee.org/abstract/document/10750945/