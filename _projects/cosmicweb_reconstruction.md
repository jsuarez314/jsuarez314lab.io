---
layout: project_single
title:  "Reconstructing the Cosmic Web"
categories: projects
---

Machine learning has made its way into observational cosmology by addressing one of the most significant challenges in the field: inferring the large-scale distribution of Dark Matter (DM) in the local Universe. The spatial distribution of DM cannot be directly observed and must be inferred from observational data. In this project, we will demonstrate how machine learning methods can assist in solving this task.

  <span style="display:block;text-align:center">![Cosmic Web Reconstruction](/assets/images/density.png)</span>

A first approximation involves matching the beta-skeleton of the galaxy distribution with the cosmic web of the DM density field.

We will present preliminary results from our reconstruction efforts based on the Illustris-TNG (TNG) simulation and discuss their implications and strategies for improvement.

For more details on how the method works, please refer to the information available in the GitHub [repository][repository]. Additional information can be found in this [paper][paper].
   

[repository]: https://github.com/jsuarez314/cosmicweb_bsk 
[paper]: https://arxiv.org/pdf/2108.10351.pdf