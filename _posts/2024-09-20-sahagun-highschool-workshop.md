---
layout: page
title:  "Workshop - Inteligencia Artificial: Ciencia, Tecnología y Educación"
date:   2024-09-20 08:00:00 -0500
categories: posts
lang: es
---

El pasado 20 de septiembre se celebró en la Escuela Normal Superior Lácides Iriarte del municipio de Sahagún, Córdoba, el **V Simposio de Ciencias Naturales y Educación Ambiental**: *"La Inteligencia Artificial: Nuevo reto de la educación"*, en el que participé como ponente con la charla **"Inteligencia Artificial: Ciencia, Tecnología y Educación"**.

<br>
<div align=center>
<img src="/assets/images/vsimposiosahagun_ia_photo.jpeg" width="70%">
</div>
<br> 

Mi participación se dividió en dos sesiones. La primera fue un seminario en el que expuse cómo la Inteligencia Artificial ha impactado sectores como la investigación, la transformación industrial y el aprendizaje en sus diferentes niveles, con ejemplos prácticos y reales.

<br>
<div align=center>
<img src="/assets/images/vsimposiosahagun_ia.png" width="70%">
</div>
<br>

La segunda sesión fue un workshop virtual en el que expliqué cómo reentrenar desde cero un modelo de inteligencia artificial para la detección de objetos en imágenes. El propósito de este taller fue brindar herramientas a los profesores, centradas en el diseño de modelos basados en inteligencia artificial para su implementación en proyectos de aula.

Si te interesa saber más acerca de este workshop, puedes acceder a la [presentación] o ver la [grabación]. También puedes visitar el repositorio en [GitHub] para obtener más información sobre los notebooks y el código diseñados para el workshop.

[presentación]: https://docs.google.com/presentation/d/1SAY7C0oNyarjaPvQn_ciqnZSsyRHd82WLvh7R7hw3hk/edit?usp=sharing

[grabación]: https://www.youtube.com/watch?v=p4Td0GNGvZQ&t=6s

[github]: https://github.com/jsuarez314/IA_Sahagun_School