---
layout: page
title:  "Inteligencia Artificial: Deep Learning con Python"
date:   2023-12-01 17:00:00 -0500
categories: posts
lang: es
---

El pasado mes de Noviembre, un grupo de estudiantes de la [Universidad Politécnica de Santa Rosa Jáuregui (UPSRJ)](https://upsrj.edu.mx/) realizó una visita a la [Universidad de América](https://www.uamerica.edu.co/) en el marco de un convenio de intercambio internacional institucional. Los estudiantes vinieron a Bogotá para participar en el curso **Inteligencia Artificial: Deep Learning con Python**.

Como resultado del curso, diseñé y desarrollé los siguientes módulos:

#### Módulo1 : Matemáticas para Inteligencia Artificial

- Algebra lineal y cálculo para IA.
- Conceptos fundamentales de estadística y probabilidad para IA.
- Regresión lineal simple y polinomial.

#### Módulo 2: Introducción al Deep Learning

- Introducción al deep learning.
- Redes neuronales artificiales.
- Imágenes como tipos de datos.
- Exploración de metaparametros.
- Redes neuronales convolucionales (CNN).
- Transferencia de aprendizaje (Transfer learning).

Los módulos, que están disponibles en [GitHub][github], se desarrollan en notebooks de Jupyter, los cuales son de acceso público y pueden ser utilizados para abordar diversos conceptos matemáticos y realizar ejercicios orientados a comprender los fundamentos del Deep Learning.

[github]: https://github.com/jsuarez314/DLPythonFUA_school