---
layout: page
title:  "Stats Covid-19 Colombia"
date:   2020-06-15 17:00:00 -0500
categories: posts
lang: en
---


The next Figure shows the spread of the Covid-19, this Figure show the Colombian situation in comparison with other countries.

  ![Global Covid](https://gitlab.com/jsuarez314/covid-19/-/raw/master/covid-19.png)


The number of positive cases detected in Colombia are represented in the next map.

  ![Colombian Map](https://gitlab.com/jsuarez314/covid-19/-/raw/master/Colombia.png)


Also, it is visualized the cases by age.

  ![Colombian By Age](https://gitlab.com/jsuarez314/covid-19/-/raw/master/Cases_by_age.png)


The number of positive cases detected in Bogotá are represented in the next map.

  ![Bogotá Map](https://gitlab.com/jsuarez314/covid-19/-/raw/master/Bogota.png)


Also for Bogotá, it is visualized the cases by age.

  ![Bogotá By Age](https://gitlab.com/jsuarez314/covid-19/-/raw/master/Cases_by_age_bog.png)

<center>
  <a class="lecture-detail" href="https://gitlab.com/jsuarez314/covid-19">View on Gitlab</a>
</center>