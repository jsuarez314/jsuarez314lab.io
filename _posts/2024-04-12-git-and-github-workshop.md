---
layout: page
title:  "Taller de Git y GitHub - RECA"
date:   2024-04-12 17:00:00 -0500
categories: posts
lang: es
---

Como parte de las actividades del comité de mentorías de la [Red de Estudiantes Colombianos de Astronomía (RECA)](https://www.astroreca.org/), durante los meses de Marzo y Abril se desarrolló el **Taller de Introducción a Git y GitHub**. Este taller estuvo dirigido a fomentar habilidades colaborativas en proyectos tecnológicos con el uso de Git y GitHub así como el diseño de sitios web como herramientas de presentación de perfiles.

El taller se dividió en dos módulos: *Introducción a Git y Control de Versiones* e *Introducción al Desarrollo y Diseño de Páginas Web con GitHub*. A través de [GitHub][github] se puede acceder tanto a las diapositivas como a las grabaciones de cada sesión. Este material es de acceso público y puede ser reutilizado con fines académicos. 

#### Módulo 1: Introducción a Git y Control de Versiones

El primer módulo del taller se centró en el desarrollo de habilidades en el uso de las herramientas Git y GitHub para la colaboración en proyectos.

- **Sesión 1:** *Introducción a Git y Control de Versiones*. [[Presentación](https://docs.google.com/presentation/d/1s_evivUa4Mas7yB1pwPr2XDAIrOHNHeEmvrNrQydbYA/edit?usp=sharing)] [Grabación]

- **Sesión 2:** *Trabajo con Git, Ramas y Fusiones en Git*. [[Presentación](https://docs.google.com/presentation/d/1zFUvLAQmnspy1pJ0Ca6KEkV9r1q05ARXWaQ85TNbzkY/edit?usp=sharing)] [Grabación]

- **Sesión 3:** *GitHub y Colaboración en Proyectos*. [[Presentación](https://docs.google.com/presentation/d/1acRGoaexMfWMCGj5Rzpwy2sWK14cFcuL_teNk4v2rWQ/edit?usp=sharing) [Grabación]

- **Sesión 4:** *Proyecto Práctico y Buenas Prácticas*. [[Presentación](https://docs.google.com/presentation/d/1rCYnf3rP4yu47jKye9E2nTybEwBlTCJWOueF6lo4qFw/edit?usp=sharing)] [Grabación]

#### Módulo 2: Introducción al Desarrollo y Diseño de Páginas Web con GitHub

El segundo módulo del taller se centró en el desarrollo web y diseño de páginas estáticas utilizando GitHub como plataforma colaborativa.

- **Sesión 5:** *Fundamentos del Desarrollo Web Estático con Jekyll y Markdown*. [[Presentación](https://docs.google.com/presentation/d/1L0KnTyoUJ6mdaEnfOOIjuyFXLQqEaCAfZms8V2Ur-KU/edit?usp=drive_link)] [Grabación]

- **Sesión 6:** *Desarrollo Web Estático Avanzado con Jekyll y GitHub Pages*. [[Presentación](https://docs.google.com/presentation/d/1ktosvRrzEK90OTaig3gfpiCd4nLESSZPF8Xb9oC7-hM/edit?usp=sharing)] [Grabación]

¡Pronto estaré compartiendo los enlaces a las grabaciones en YouTube de cada sesión!

[github]: https://github.com/jsuarez314/git-workshop