---
layout: project_single
title:  "Thermodynamic Modeling in Bioengineering and Chemical Processes [Computational] (2025-1)"
categories: teaching
---

## Introduction to Matrices

- (Slides) [Introduction to matricial notation and system of linear equations](https://docs.google.com/presentation/d/1V13fycqrcwrJ-yLgmbcnbkOcGiIo3KpX6wPQ_s0P-LM/edit?usp=sharing) [es]
	- (Matlab Script) [Graphic Method to solve system of linear equations](https://drive.google.com/file/d/197Srnxo7wQTakfa8K_lBeL88Pzrl7Lsv/view?usp=sharing)
	- (Matlab Script) [Matricial Notation & Operations](https://drive.google.com/file/d/196IiJ1MHMoZk2vGBdON5K0E4g1-8Ddua/view?usp=sharing)

## Matricial Operations

- (Slides) [Matricial operations](https://docs.google.com/presentation/d/1EQhIpW9zLgAmahu77qdd3Efpvm6ZpThzyZimvS3xdlc/edit?usp=sharing) [es]
	- (Matlab Script) [Matricial operations](https://drive.google.com/file/d/1TBIV6H-xs2J8ttxy_x2z6DTgMB0CS2gM/view?usp=sharing)

## Gauss-Jordan Method

- (Slides) [Gauss-Jordan Method](https://docs.google.com/presentation/d/1nyt2e7ZJS-RgkHYecyeFahA6LLrblFH4fqDSXHmofnQ/edit?usp=sharing) [es]
	- (Matlab Script) [Gauss-Jordan Method](https://drive.google.com/file/d/1CcMRmBibTzZlvUXjEeyR-nlwcEzIXtRp/view?usp=sharing)